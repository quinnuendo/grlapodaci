Programi za rad sa grlima
=========================

Kolekcija nekoliko programa koji sluze za rad sa podacima o životinjama.

- FixId
- Reorganizacija
- RTFPedigrei
- SpisakOdPedigrea
- DodajGeneracije
- SlikeGeneologije
- PorekloBaza
- Fiksne Sirine

Neki od programa su opisani dodatnim dokumentima u folderu "dokumentacija".

Niže su opisani načini pokretanja, potrebne instalacije, način rada sa opcijama
i slično.


Operacije
=========

FixId
------

Program sluzi da ucita podatke iz CSV fajla o trkama konja u okviru kojih
su id konja, oca i majke, kao i pol životinje. Program potom menja id-ove tako 
da deca uvek imaju veci broj od svojih roditelja.

Kolone za ove vrednosti se podrazumevaju da su:

- prva - id konja
- druga -pol konja
- sedma - ID oca
- osma - ID majke

Ove vrednosti se mogu promeniti u fajlu "opcije.txt" u
folderu gde je i program i odakle se pokrece.
Na primer mogu se dodati (ili promeniti ako vec postoje)
redovi u kome pise:

```
KolonaId=1
KolonaPol=2
KolonaIdOtac=3
KolonaIdMajka=4
```

Ako neki od ovih nedostaje, podrazumevaju se vec gore navedene
vrednosti. U opcijama se podrazumeva da brojanje kolona
krece od 1.

Izlaz je u novom fajlu sa nastavkom "-renum" u kome su dodate tri
nove kolone na kraj fajla u kojima su novi id-ovi.

Takodje se ispisuje fajl "renumeracija.txt" u kome se nalaze u
svakom redu po dva broja razdvojeni ';' koji predstavljaju stari
id i novi id.

Informativno se ispisuje i 'povezanost.txt' u kome se nalaze podaci
o nadjenoj mrezi roditelja i dece. Ispisuju se podaci u sledecem formatu:

    Grlo:2138	Otac:n966	Majka:n486	deca:[2135, 2143, 2157]

kod roditelja se pre id-a nalazi 'd' ili 'n' odnosno da li postoje unosi 
o njima, ili su idovi van baze. Dalje su izlistana nadjena deca.

U slucaju gresaka u podacima, program ih prijavljuje u komandnoj liniji.

Za roditelje koji nisu u bazi se pamti pol radi provere da se negde ne pojavljuje
isto grlo nekom kao otac, nekom kao majka. Specijalno za id jednak sa nulom i
za negativne id brojeve se smatra da su nepoznati roditelji i za njih se ne
proverava da li je stalno isti pol.


Reorganizacija
--------------

Program je predviđen za reorganizaciju CSV fajla za potrebe
ocenjivanja značaja više odobina. Početni fajl je takav da u svakom
redu ima jedan "događaj", odnosno kolekciju podataka. Potom se on
reorganizuje tako da se od jednog reda napravi više njih, a u svakom
je po jedan podatak koji se ocenjuje iz originalnog reda.

Originalno je program napravljen da radi sa podacima sa trka konja,
ali može da funkcioniše sa bilo kakvim podacima ako su ispravno
organizovani.

Iz fajla 'opcije.txt' se ucitavaju podaci o tome koje kolone su
bitne za ocenjivanje, a koje treba proceniti kako uticu na njih.

Fajl treba da izgleda ovako:

```
osobine=Sekundi;km\h;1st
uticaji=Grlo;Pol;Otac;Majka;Starost
```

Bitno je da postoje dva reda koji pocinju kao i u primeru, a nakon
toga se navodi spisak kolona kakve postoje u zaglavlju fajla razdvojene
sa ';'.

Izlaz ce se naci u novom fajlu sa nastavkom "-bi". U njemu ce se za
svaki red iz originalnog fajla napraviti po jedan za svaku osobinu
koja je zadata. U svakom od tih redova se prepisuju svi podaci koji su
izlistani u opciji "uticaji". Prva kolona je broj osobine (dodeljuju
se redom), a poslednja kolona je vrednost osobine.

U fajl 'brojeviOsobina.txt' se ispisuju osobine i njihovi brojevi.

Postoji primer za ovo u folderu primeri.


RTFPedigrei
-----------

Program sluzi da iz kolekcije RTF fajlova koji predstavljaju 
pojedinacne konje i njihov pedigre napravi tabelu sa tim podacima. 

Programu se daje folder u kome su fajlovi, a on ce napraviti .csv fajl
sa imenom tog foldera i svim podacima koje je uspeo da potpuno popuni. 

Polja se uzimaju na osnovu broja reda i koliko ih tabulatora deli od 
pocetka. Zbog nekonzitencije fajlova je moguce da ce postojati 
brojni problemi i da podaci nisu uvek pouzdani. Program zbog toga 
usput prijavljuje sve sto nije mogao da nadje, a nakon toga ispisuje 
i ime problematicnog fajla koji je bio obradjivan da bi se problemi 
mogli vezati za konkretne fajlove.

Bilo koji fajl za koji program nije nasao sva polja (makar i prazna) 
će biti ispisani u drugi fajl koji se zove kao folder sa dopunom 
"-nepotpuni".

Takodje se moze pozivati i prvom opcijom "--single" tako da obradi samo
jedan fajl i ispise kako ga je video i da li je bilo problema.

Primer rasporeda podataka u RTF fajlu je dat u "primer/prototip.rtf" u
kome su ispisana polja koja se prepoznaju na adekvatnim mestima. Takodje
su dopisani brojevi redova za sve u kojima se nalazi neki podatak. Osim
podataka koji se prepoznaju takodje postoje i podaci o zemlji porekla
grla, kao i vremenima na trkama. Ovi podaci se ignorisu od strane 
programa. Specijalno za poslednju generaciju predaka se ime nalazi odmah
praceno poreklom i vremenom (a ne u novom redu), pa se ovi dodatni
podaci odsecaju. Tacnije za ovu generaciju se odseca sve posle razmaka 
pre poslednje otvorene zagrade, cime se omogucava da ostane i naziv u 
kome postoji zagrada.


SpisakOdPedigrea
----------------

Program sluzi da od CSV-a koji sadrzi generacije životinja (dobijen
najverovatnije koriscenjem RTFPedigrei) napravi spisak životinja 
sa roditeljima koje se mogu naci. 

Prave se dva izlaza, jedan su ispisana sva imena i generisani id
brojevi.

Drugi izlaz je CSV sa kolonama "id", "pol", "idOtac" i "idMajka".

Program ce upozoravati za sve životinje kod koji nadje razlicite
definicije roditelja na razlicitim mestima, ali ce ostati snimljena
prva nadjena. Ako je roditelj bio nepoznat pre, onda ce biti dopunjen,
a i tome ce biti ispisana poruka.

Nepoznati roditelji bi u rezultujucem fajlu trebali da zavrse
kao negativne vrednosti, tacnije grla oznacena sa " " ce dobiti id -1,
"" (prazan string) ce biti -2, a "?" upitnik ce biti -3.

Takodje ce grla sa imenom 0 dobiti id 0.

Broj generacija koje ce se pretrazivati kod svakog grla se moze 
kontrolisati opcijom "SpisakMaxGeneracija" u fajlu "opcije.txt". 
Podrazumevana vrednost je "3", a ispisuje se i prilikom pokretanja 
programa.


DodajGeneracije
---------------

Ovaj program prima spisak grla sa id-ovima roditelja i potom ispisuje 
dodatne podatke o pedigreu grla u novi CSV. Trenutno se dopisuju 
sledece kolone: "O", "OO" i "OMO" u znacenju: otac, očev otac i očeve 
majke otac, respektivno.

Postojeci podaci iz originalnog fajla sa prepisuju u novi fajl, a podaci
se dodaju kao nove kolone na kraj.

Koriste se podaci id, idOtac i idMajke na osnovu kolona iz "opcije.txt",
kao i kod FixId programa.


SlikeGeneologije
-------------

Program generiše graf povezanosti za grla po ženskoj liniji
Očekuje da su u fajlu kolone:
- Grlo, id
- Majka, id
- Godina rođ
- GME grlo
- Komplet podaci o grlu za ispis
- Komplet za oca
- Komplet za majku

Na osnovu GME se određuje šta ide na crtež. Ako je unutra "O" onda je
to osnivač. Očekuje se samo jedan osnivač u fajlu. Svi kod kojih GME
sadrži "RB" ili "HB" će biti ciljna grla do kojih će se tražiti linija
do osnivača. Na crtežu će se naći po potrebi sva grla koja ih
povezuju, a ostala će biti ignorisana. Ako neko od ciljnih grla ne
može da se poveže ispisaće se greška na ekran, ali generisaće se slika
koliko je moguće.

Grlo osnivač će biti označeno specijalno, kao i ciljna grla.

Program generiše graf u "dot" formatu, poznatom preko programa
GraphViz.  Iz ovoga se onda može generisati crtež preko nekog od
mnogih programa koji rade sa ovim formatom. Rezultat može biti png,
svg (vektorska slika), pdf i tako dalje, zavisi od podrške konkretnog
programa.

Slika se može prilagoditi sa dodatnim opcijama i fajlom. Dodatne opcije
se mogu dati pre imena ulaznog fajla.

Opcija `--a4` će ubaciti na početak grafa direktive da se graf smesti
na A4 stranicu. Na primer:

   SlikeGeneologije --a4 ulaz.csv izlaz.dot

Za preciznije upravljanje ispisom moguće je precizirati mnogo tačnije kako
se pojedini delovi ispisuju.  Tačnije moguće je tačno opisati kako se
ispisuje osnivač, kako se ispisuju aktivna grla, te kako se ispisuju sva
ostala, "normalna" grla.  Dodatno se mogu precizirati i bilo kakve dodatne
opcije koje će biti prepisane na početak izlaznog fajla.  Ovako se omogućava
da postoji nekoliko raznih formata koji se menjaju po potrebi, a bez ikakvih
promena u programu.

Opcija `-f format-fajl.txt` kaže da se iz datog fajla učitaju opcije
za formatiranje grafa i pojedinačnih tipova čvorova. Fajl se sastoji
iz sledećih redova:

- prvi red je formatiranje za grlo osnivača
- drugi red je za "normalna" grla
- treći red je za aktivna grla, odnosno ona zadata kao ciljevi u fajlu
- sve nakon toga se koristi na početku grafa i može sadržati opcije
kao što je formatiranje stranice, smer crtanja grafa i bilo šta drugo
dozvoljeno u formatu.

Svaki od tih redova za grlo dozvoljava kompletno upravljanje prikazom čvora
u grafu.  Specijalnim znacima se označava gde će biti stavljena imena za
pojedinačno grlo i eventualno roditelje.  U okviru tog reda se obavljaju
sledeće zamene za konkretno grlo:

- %2s podaci o grlu
- %3s podaci o ocu
- %4s podaci o majci

Ne moraju se koristiti svi podaci, podrazumevana verzija ne koristi
podatke o majci trenutno.

Dati su neki primeri fajlova za ulaz sa imenima `format-*`.

Primer upotrebe:

    SlikeGeneologije -f format-nadesno.txt ulaz.csv izlaz.txt


Neki od programa
- zvanični program GraphViz (http://graphviz.gitlab.io/)
- https://sketchviz.com/
- https://graphs.grevian.org/graph/
- https://stamm-wilbrandt.de/GraphvizFiddle/

Alternativno se može koristiti i za muške linije, ako se samo zamene
polovi adekvatno, tj program će svejedno tražiti preko majke, pa treba
tamo staviti oca i rotirati adekvatno dodatne podatke za crtež.


PorekloBaza
-------------

Program nalazi poreklo (povezane predtke) od zadatih baznih grla.

Videti detalje u odvojenom fajlu u folderu "dokumentacija".


FiksneSirine
--------------

Program pretvara dati fajl u CSV formatu (razdvojen zarezima) u fajl
sa fiksnim širinama kolona.

Videti detalje u odvojenom fajlu u folderu "dokumentacija".


Pokretanje
==========

Za program je potrebna Java Virtuelna Masina (JVM). Ako nije instalirana,
moze se naci na zvaninom sajtu (https://www.java.com/getjava). 

Programi su kompajlirani tako da rade sa verzijom JVM 8 i novijima.


Pokretanje se vrsi sa

    java -jar grla-podaci.jar <operacija> <opcije>

pri cemu <operacija> moze biti jedno od:

- FixId
- Reorganizacija
- RTFPedigrei
- SpisakOdPedigrea
- DodajGeneracije
- SlikeGeneologije
- PorekloBaza
- FiksneSirine

Opcije zavise od izabrane operacije. Uglavnom
je samo u pitanju fajl koji ce biti obradjen.
Pozivom bez opcija se dobija tekst pomoci.

Alternativno se mogu koristiti prilozeni bat fajlovi.

- FixId.bat 'imefajla'
- Reorganizacija.bat 'imefajla'
- RTFPedigrei.bat 'folder'
- SpisakOdPedigrea.bat 'imefajla'
- DodajGeneracije.bat 'imefajla'
- SlikeGeneologije.bat [opcije] 'imeUlaznog' 'imeIzlaznog'
- PorekloBaza.bat 'imepedigrea' 'imebaza'
- FiksneSirine.bat 'imefajla'


Opcije
=======

U fajlu "opcije.txt" se drze opcije za neke od programa u kolekciji, a
neke su i deljene medju njima. Ako fajl ne postoji, uvek postoje
podrazumevane vrednosti.

Primer kako fajl moze da izlgeda je dat u "primer-opcije.txt". On
se moze koristiti preimenovanjem u "opcije.txt".


Primeri
=======

U folderu primeri je dato nekoliko primera ulaznih fajlova,
kao i njihovih izlaza. Za vise detalja pogledati README.txt
u tom folderu.


Ulazni fajlovi
==============

Mnoge operacije ocekuju da je ulazni fajl CSV - "Comma Separated Values",
odnosno da su vrednosti u fajlu razdvojene zarezima. Ukoliko
su originalni fajlovi u formatu xsl(x) mogu se izvesti iz
Excella, ali je moguce da u zavisnosti od regionalnih podesavanja izlaz
u CSV ne bude razdvojen zarezima, vec na primer sa ";". To se
moze prepraviti u okviru regionalnih podesavanja. Alternativno
se moze koristiti OpenOffice ili LibreOffice koji nude opcije
za izvoz direktno u okviru programa i podrazumevano izvoze
sa zarezima.

Za ove fajlove se očekuje da su izvezeni sa enkodingom (način na koji se
tumače slova) "UTF-8". Za mnoge fajlove ovo nije previše važno, ako su
unutra samo brojevi i "engleska" slova. Međutim, čim se pojave dijakritici
(kao što su č,ć) moguće je da dođe do "čudnih" izlazinih fajlova.


Kod RTF fajlova se ocekuju fajlovi sa enkodingom "windows-1252", posto
su takvi bili pri obradi podataka.


Licence i biblioteke
====================

Ovaj program se distribuira pod GNU javnom licencom - GPLv3 ili kasnija.

Program u okviru sebe koristi biblioteku OpenCSV koja je dostupna
pod Apache licencom. Biblioteka se nalazi u folderu 'lib'.
http://opencsv.sourceforge.net/
