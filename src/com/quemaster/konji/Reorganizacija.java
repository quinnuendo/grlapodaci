package com.quemaster.konji;
/*
Copyright (C) 2014, 2015  Doni Pracner

 This file is part of grla-id-fix.

 grla-id-fix is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 grla-id-fix is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with grla-id-fix.  If not, see <http://www.gnu.org/licenses/>.
*/
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class Reorganizacija {
	
	private static final String OPCIJE_OSOBINE = "osobine";
	private static final String OPCIJE_UTICAJI = "uticaji";
	private String[] osobine;
	private String[] uticaji;
	
	private void prosiriUNoviFajl(String stari, String novi) {
		try {
			System.out.println("otvaramo fajl "+stari);
			System.out.print("osobine za evaluaciju:");
			for (String s:osobine) 
				System.out.print("'"+s+"' ");
			System.out.println();
			System.out.print("osobine koji su uticaji:");
			for (String s:uticaji) 
				System.out.print("'"+s+"' ");
			System.out.println();			CSVReader reader = new CSVReader(new FileReader(stari));
			CSVWriter writer = new CSVWriter(new FileWriter(novi));

			String[] header = reader.readNext();
			
			//napravimo novi header
			int duzina = uticaji.length + 2;
			String[] iheader = new String[duzina];
			for (int i=1; i<duzina-1;i++)
				iheader[i] = uticaji[i-1];
			iheader[0] = "Osobina";
			iheader[duzina - 1] = "Vrednost";
			
			writer.writeNext(iheader);
			
			int[] koloneUticaji = new int[uticaji.length];
			for (int i=0; i<uticaji.length;i++){
				koloneUticaji[i] = Arrays.asList(header).indexOf(uticaji[i]);
				if (koloneUticaji[i]<0){
					System.err.println("greska: kolona nije nadjena u zaglavlju fajla: '"+uticaji[i]+"'");
					System.err.println("kukavicki odustajem od daljeg rada!");
					System.exit(1);
				}
			}
			
			int[] koloneOsobine= new int[osobine.length];
			for (int i=0; i<osobine.length;i++){
				koloneOsobine[i] = Arrays.asList(header).indexOf(osobine[i]);
				if (koloneOsobine[i]<0){
					System.err.println("greska: kolona nije nadjena u zaglavlju fajla: '"+osobine[i]+"'");
					System.err.println("kukavicki odustajem od daljeg rada!");
					System.exit(1);
				}
			}
			
			String[] nextLine;
			int c = 0;
			while ((nextLine = reader.readNext()) != null) {
				String[] izlaz = new String[uticaji.length + 2];
				for (int i=0; i<uticaji.length;i++)
					izlaz[i+1] = nextLine[koloneUticaji[i]]; 
				for (int i=0; i<osobine.length;i++){
					izlaz[0] = (i+1)+"";
					izlaz[duzina-1] = nextLine[koloneOsobine[i]];
					writer.writeNext(izlaz);
				}
				c++;
			}
			reader.close();
			writer.close();
			System.out.println("Obradjeno redova:" + c);
			System.out.println("Rezultati snimljeni u fajl:"+novi); 
		} catch (IOException e) {
			System.err.println("Greska pri pisanju iz fajla:" + stari
					+ " u fajl:" + novi);
			e.printStackTrace();
		}
	}
	

	
	public void run(String[] args){
		if (args.length == 0){
			pomoc();
			System.exit(0);
		}
		String fajl = args[0];
		if (args[0].compareToIgnoreCase("--demo")==0)
			fajl = Glavni.DEF_FAJL;
		Glavni.ucitajOpcije();
		srediKolone();
		prosiriUNoviFajl(fajl, Glavni.napraviNovoIme(fajl,"-bi"));
		PrintWriter rout;
		try {
			rout = new PrintWriter("brojeviOsobina.txt");
			ispisiOsobine(rout);
			rout.close();
		} catch (FileNotFoundException e) {
			System.err.println("greska pri pisanju fajla sa osobinama");
		}
	}
	
	public void ispisiOsobine(PrintWriter out){
		for (int i=0; i<osobine.length; i++){
			out.println((i+1)+";"+osobine[i]);
		}
	}
	
	public void pomoc() {
		System.out.println("Program Reorganizacija;");		
		System.out.println("Fajl koji se obradjuje se zadaje iz komandne linije:");
		System.out.println("\tReorganizacija imefajla.csv");
		System.out.println("Izlaz:");
		System.out.println("- 'imefajla-bi.csv' osobine su reorganizovane u nove redove ");
		System.out.println("- 'brojeviOsobina.txt' sadrzi brojeve za osobine u novom fajlu");
		System.out.println();
		System.out.println("Program prima CSV sa podacima o trkama konja. Na osnovu podataka datih");		
		System.out.println("u opcije.txt se fajl reorganizuje da se ispitaju te osobine");		
		System.out.println("Autor Doni Pracner");
		
	}
	private void srediKolone() {
		//uzimamo iz opcija podatke o tome koje kolone nam trebaju a koje ne
		osobine = Glavni.Opcije().getProperty(OPCIJE_OSOBINE,"").split(";");
		uticaji = Glavni.Opcije().getProperty(OPCIJE_UTICAJI,"").split(";");
	}

	public static void main(String[] args){
		new Reorganizacija().run(args);
	}

}
