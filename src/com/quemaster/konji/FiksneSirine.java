package com.quemaster.konji;
/*
Copyright (C) 2022  Doni Pracner

 This file is part of grla-id-fix.

 grla-id-fix is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 grla-id-fix is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with grla-id-fix.  If not, see <http://www.gnu.org/licenses/>.
*/
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

import au.com.bytecode.opencsv.CSVReader;

public class FiksneSirine {
	private static final String OP_SEPARATOR = "Separator";
	private static final String OP_KOLONE = "Kolone";
	private char separator = ',';
	private ArrayList<Integer> sirineKolona;

	public void run(String[] args) {
		// TODO pomoc razraditi i slicno
		if (args.length == 0) {
			pomoc();
			System.exit(0);
		}
		Glavni.ucitajOpcije();
		namestiOpcije();

		String fajl = args[0];
		prebaciUFixedWidth(fajl, Glavni.napraviNovoIme(fajl, "-fiksne"));
	}

	private void pomoc() {
		System.out.println(
				"Program za pretvaranje CSV fajla u fajl sa fiksnim sirinama");
		System.out.println("Fajl koji se obradjuje se zadaje iz komandne linije:");
		System.out.println("\tFiksiraneSirine imefajla.csv");
		System.out.println("Izlaz:");
		System.out.println("- 'imefajla-fiksne.csv' kao ulaz, sa zadatim sirinama kolona ");
		System.out.println("Sirine kolona se zadaju u opcije.txt");
		
	}

	private void prebaciUFixedWidth(String fajl, String izlaz) {
		try {
			CSVReader reader = new CSVReader(new FileReader(fajl), separator);
			PrintWriter out = new PrintWriter(izlaz);
			String[] nextLine;
			int c = 0;
			System.out.println(sirineKolona);
			// reader.readNext();// preskacemo header
			while ((nextLine = reader.readNext()) != null) {
				int i = 0;
				for (String s : nextLine) {
					out.print(
							String.format("%-" + sirineKolona.get(i) + "s ", s));
					if (s.length() > sirineKolona.get(i))
						System.err.println("Upozorenje, presirok podatak:\"" + s
								+ String.format(
										"\"; red: %1d polje: %2d, zadata sirina: %3d",
										c + 1, i + 1, sirineKolona.get(i)));
					i++;
				}
				out.println();
				c++;
			}
			reader.close();
			out.close();
			System.out.println("Ucitano redova:" + c);
		} catch (IOException e) {
			System.err.println("Greska pri citanju fajla:" + fajl);
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void main(String[] args) {
		new FiksneSirine().run(args);
	}

	private void namestiOpcije() {
		String s = Glavni.Opcije().getProperty(OP_SEPARATOR);
		if (s != null && s.length() > 0) {
			separator = s.charAt(0);
			if (s.length() > 1)
				System.err.println(
						"Upozorenje: separator iz opcija duzi od 1 znaka; uzet samo prvi");
		}
		s = Glavni.Opcije().getProperty(OP_KOLONE);
		if (s != null) {
			String[] ss = s.split(",");
			sirineKolona = new ArrayList<Integer>(ss.length);
			for (String a : ss)
				sirineKolona.add(Integer.parseInt(a));
		} else {
			sirineKolona = new ArrayList<>();
			sirineKolona.addAll(Collections.nCopies(15, new Integer(5)));
		}
	}

}
