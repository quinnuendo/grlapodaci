/*
 Copyright (C) 2015 Doni Pracner

 This file is part of grla-id-fix.

 grla-id-fix is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 grla-id-fix is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with grla-id-fix.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quemaster.konji;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.TreeMap;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class SpisakOdPedigrea {
	
	public static final int MAX_GEN_LENGTH = 3;

	public static final String OPCIJE_MAXLENGTH = "SpisakMaxGeneracija";
	
	private static int maxGenLength = 3;

	private HashMap<Long, Grlo> grla = new HashMap<>();
	
	private TreeMap<String, Long> idovi = new TreeMap<>();
	private HashMap<Long, String> imena = new HashMap<>();
	
	private TreeMap<String, Integer> kolone = podrazumevaneKolone();

	private long nextAvailableId=1;
	private String[] generacije = new String[] {
			"o", "m", 
			"oo", "om", "mo", "mm", 
			"ooo", "oom", "omo", "omm",
			"moo", "mom", "mmo", "mmm",
			"oooo", "ooom", "oomo", "oomm",
			"omoo", "omom", "ommo", "ommm",
			"mooo", "moom", "momo", "momm",
			"mmoo", "mmom", "mmmo", "mmmm",
			};
	
	private String[] header = new String[] {
			"Id", "Pol", "idOtac", "idMajka" 
			};
	
	private TreeMap<String, Integer> podrazumevaneKolone() {
		TreeMap<String,Integer> kol = new TreeMap<String,Integer>();
		kol.put("ime", 0);
		kol.put("pol", 1);
		kol.put("o", 7);
		kol.put("m", 8);
		kol.put("oo", 9);
		kol.put("om", 10);
		kol.put("mo", 11);
		kol.put("mm", 12);
		kol.put("ooo", 13);
		kol.put("oom", 14);
		kol.put("omo", 15);
		kol.put("omm", 16);
		kol.put("moo", 17);
		kol.put("mom", 18);
		kol.put("mmo", 19);
		kol.put("mmm", 20);
		kol.put("oooo", 21);
		kol.put("ooom", 22);
		kol.put("oomo", 23);
		kol.put("oomm", 24);
		kol.put("omoo", 25);
		kol.put("omom", 26);
		kol.put("ommo", 27);
		kol.put("ommm", 28);
		kol.put("mooo", 29);
		kol.put("moom", 30);
		kol.put("momo", 31);
		kol.put("momm", 32);
		kol.put("mmoo", 33);
		kol.put("mmom", 34);
		kol.put("mmmo", 35);
		kol.put("mmmm", 36);
		return kol;
	}

	public void ucitajPedigreeIzFajla(Path fajl) {
		try {
			CSVReader reader = new CSVReader(Files.newBufferedReader(fajl));
			String[] nextLine;
			int c = 0;
			reader.readNext();// preskacemo header
			while ((nextLine = reader.readNext()) != null) {
				ubaciRedUStablo(nextLine);
				c++;
			}
			reader.close();
			System.out.println("Ucitano redova:" + c);
		} catch (IOException e) {
			System.err.println("Greska pri citanju fajla:" + fajl);
			e.printStackTrace();
			System.exit(1);
		}
	}

	private void ubaciRedUStablo(String[] nextLine) {
		long id = nadjiId(nextLine[kolone.get("ime")]);
		
		int pol = zakljuciPol(nextLine[kolone.get("pol")]);
		
		HashMap<String,Long> mapa = new HashMap<>();
		for (String s : generacije){
			mapa.put(s, nadjiId(nextLine[kolone.get(s)]));
		}
		
		mapa.put("ime", id);
		try {
			ubaciGrloUStablo(id, mapa.get("m"), mapa.get("o"), pol);
		} catch (Exception e) {
			System.err.println("Greska: iz pedigra grla:"+imena.get(mapa.get("ime")));
			System.err.println(e.getMessage());
			System.err.println();
		}
		
		ubaciIzMape(mapa,"m");
		ubaciIzMape(mapa,"o");
	}

	private void ubaciIzMape(HashMap<String, Long> mapa, String gen) {
		int pol=Grlo.POL_M;
		if (gen.endsWith("m")) 
			pol = Grlo.POL_Z;
		try {
			ubaciGrloUStablo(mapa.get(gen),mapa.get(gen+"m"), mapa.get(gen+"o"), pol);
		} catch (Exception e) {
			System.err.println("Greska: iz pedigra grla:"+imena.get(mapa.get("ime")));
			System.err.println(e.getMessage());
			System.err.println();
		}
		if (gen.length()<maxGenLength-1) {
			ubaciIzMape(mapa,gen+"m");
			ubaciIzMape(mapa,gen+"o");
		}
	}

	public static int zakljuciPol(String string) {
		if (string != null){
			char c = Character.toLowerCase(string.charAt(0));
			if (c=='ž') return Grlo.POL_Z;
			if (c=='m') return Grlo.POL_M;
			if (c==Grlo.POL_M+'0') return Grlo.POL_M;
			if (c==Grlo.POL_Z+'0') return Grlo.POL_Z;
		}
		return -1;
	}

	public void ubaciGrloUStablo(long id, long idm, long ido, int pol) throws Exception {
		Grlo novo = new Grlo(id,ido,idm,pol);
		Grlo g = grla.get(id);
		if (g==null){
			grla.put(id,novo);
		} else {
			if (!novo.istiIdIRoditelji(g)){
				String poruka = "";
				if (g.idOtac <= 0 && ido > 0) {
						g.idOtac = ido;
						poruka += "\n\t"+"nepostojeci otac dopunjen!";
						
				}
				if (g.idMajka <= 0 && idm > 0) {
						g.idMajka = idm;
						poruka += "\n\t"+"nepostojeca majka dopunjena";
				}
				throw new Exception("grlo vec u spisku, postoje razlike!:" +
				"\nt\t"+opisGrla(g) +
				"\nt\t"+opisGrla(novo)
				+ poruka);
			}
		}
	}

	private String opisGrla(Grlo g){
		return "Grlo:"+g.id+" ("+imena.get(g.id)+")"
		+"\tOtac"+g.idOtac+" ("+imena.get(g.idOtac)+")"
		+"\tMajka:"+g.idMajka+" ("+imena.get(g.idMajka)+")";		
	}
	
	private long nadjiId(String string) {
		Long l = idovi.get(string);
		if (l==null) {
			l = nextAvailableId++;
			idovi.put(string, l);
			imena.put(l, string);
		}
		return l;
	}
	
	public void snimiRoditelje(Path csv) {
		try {
			CSVWriter pisac = new CSVWriter(Files.newBufferedWriter(csv,
					Charset.defaultCharset()));
			pisac.writeNext(header);
			for (Grlo g : grla.values()) {
				String[] izlaz = new String[header.length];
				int i = 0;
				izlaz[i++] = g.id + "";
				izlaz[i++] = g.pol + "";
				izlaz[i++] = g.idOtac + "";
				izlaz[i++] = g.idMajka + "";
				pisac.writeNext(izlaz);
			}
			pisac.close();
			System.out.println("Grla i roditelji (ukupno " + grla.size()
					+ ") snimljeni u fajl " + csv.toString());
		} catch (IOException e) {
			System.err.println("greska u snimanju roditelja u fajl:"
					+ csv.toString());
			e.printStackTrace();
		}
	}
	
	public void snimiImenaIIdove(Path csv) {
		try {
			CSVWriter pisac = new CSVWriter(Files.newBufferedWriter(csv,
					Charset.defaultCharset()));
			pisac.writeNext(new String[] { "Ime", "id" });
			for (String s : idovi.keySet()) {
				String[] izlaz = new String[2];
				izlaz[0] = s;
				izlaz[1] = idovi.get(s) + "";
				pisac.writeNext(izlaz);
			}
			pisac.close();
			System.out.println("Id-ovi (ukupno " + idovi.size()
					+ ") snimljeni u fajl " + csv.toString());
		} catch (IOException e) {
			System.err.println("greska u snimanju roditelja u fajl:"
					+ csv.toString());
			e.printStackTrace();
		}
	}

	private void pomoc() {
		System.out.println("Program SpisakOdPedigrea;");		
		System.out.println("CSV koji se obradjuje se zadaje iz komandne linije:");
		System.out.println("\tSpisakOdPedigrea fajl.csv");
		System.out.println("Izlaz:");
		System.out.println("- 'fajl-id.csv' snima generisane id-ove od svih konja");
		System.out.println("- 'fajl-roditelji.csv' snima za sve konje id i roditelje");
		System.out.println();
		System.out.println("Ocekivani brojevi kolona (indeksirani od 0):");
		System.out.println(kolone);
		System.out.println();
		System.out.println("Autor Doni Pracner");
	}
	
	public void run(String[] args) {
		if (args.length == 0) {
			pomoc();
			System.exit(0);
		}
		
		Glavni.ucitajOpcije();
		maxGenLength = Glavni.brojIzOpcija(OPCIJE_MAXLENGTH, MAX_GEN_LENGTH);
		
		String fajl = args[0];
		// ubacujemo negativne vrednosti za moguca prazna imena
		idovi.put(" ", -1l);
		idovi.put("", -2l);
		idovi.put("?", -3l);
		// stavimo da je nula nula ako se naidje na to
		idovi.put("0", 0l);
		ucitajPedigreeIzFajla(Paths.get(fajl));
		System.out.println("Broj generacija koji je obradjivan:"+maxGenLength);
		snimiImenaIIdove(Paths.get(Glavni.napraviNovoIme(fajl, "-id")));
		snimiRoditelje(Paths.get(Glavni.napraviNovoIme(fajl, "-roditelji")));
	}

	public static void main(String[] args){
		new SpisakOdPedigrea().run(args);
	}

}
