package com.quemaster.konji;

/*
 Copyright (C) 2014, 2015, 2020, 2021  Doni Pracner

 This file is part of grla-id-fix.

 grla-id-fix is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 grla-id-fix is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with grla-id-fix.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.util.TreeSet;

public class Grlo {

	public static final int POL_Z = 1;
	public static final int POL_M = 2;

	long id = -1;
	long idOtac = -1;
	long idMajka = -1;

	int pol = -1;

	Grlo otac = null;
	Grlo majka = null;
	
	int godRodj = -1;
	
	String rasa = null;
	// zasad datum samo kao string iz fajla
	String datum = null;

	public Grlo(String id, String idOtac, String idMajka) {
		this(Glavni.longIliM1(id), Glavni.longIliM1(idOtac), Long
				.parseLong(idMajka));
	}

	public Grlo(String id, String idOtac, String idMajka, String pol) {
		this(Glavni.longIliM1(id), Glavni.longIliM1(idOtac), Glavni.longIliM1(idMajka), Glavni.intIliM1(pol));
	}

	public Grlo(long id, long idOtac, long idMajka) {
		this(id,idOtac,idMajka,-1);
	}

	public Grlo(long id, long idOtac, long idMajka, int pol) {
		super();
		this.id = id;
		this.idOtac = idOtac;
		this.idMajka = idMajka;
		this.pol = pol;

		if (id == idOtac || id == idMajka) {
			System.err.println("Greska: grlo "+ id + " samo sebi roditelj!");
		}
	}

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Grlo && id == ((Grlo) obj).id);
	}

	@Override
	public int hashCode() {
		return Long.hashCode(id);
	}

	@Override
	public String toString() {
		return "Grlo:" + id + "\tOtac:" + (otac != null ? "d" : "n") + idOtac
				+ "\tMajka:" + (majka != null ? "d" : "n") + idMajka
				+ "\tdeca:" + deca;
	}
	
	public String kratkiIspis() {
		return String.format("%d|%d|%d|%s|%d", id, idOtac, idMajka, datum, pol);
	}

	public TreeSet<Long> deca = new TreeSet<Long>();
	
	public boolean istiIdIRoditelji(Grlo grlo) {
		return (id == grlo.id && idOtac == grlo.idOtac && idMajka == grlo.idMajka);
	}
}