package com.quemaster.konji;
/*
Copyright (C) 2014, 2015, 2016, 2020, 2021, 2022  Doni Pracner

 This file is part of grla-id-fix.

 grla-id-fix is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 grla-id-fix is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with grla-id-fix.  If not, see <http://www.gnu.org/licenses/>.
*/
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class FixId {
	static final int COL_ID = 1;
	static final int COL_ID_OTAC = 7;
	static final int COL_ID_MAJKA = 8;
	static final int COL_POL = 2;
	static final int COL_RASA = -1; //obicno ne radimo sa tim
	static final int COL_DATUM = -1; //obicno ne radimo sa tim
	private static final String OPCIJE_COL_ID = "KolonaId";
	private static final String OPCIJE_COL_ID_OTAC = "KolonaIdOtac";
	private static final String OPCIJE_COL_ID_MAJKA = "KolonaIdMajka";
	private static final String OPCIJE_COL_POL = "KolonaPol";
	private static final String OPCIJE_COL_RASA = "KolonaRasa";
	private static final String OPCIJE_COL_DATUM = "KolonaDatum";
	private static final String OPCIJE_PROVERI_RASU = "ProveraRasaPorekla";
	private HashMap<Long, Grlo> grla;
	private LinkedList<Long> bezRoditelja = new LinkedList<Long>();
	private HashMap<Long, Long> renumeracija = new HashMap<Long, Long>();
	private HashMap<Long, Integer> vanBaze = new HashMap<>();
	private int colId;
	private int colOtac;
	private int colMajka;
	private int colPol;
	private int colRasa;
	private int colDatum;

	public HashMap<Long, Grlo> ucitajIzFajla(String fajl) {
		HashMap<Long, Grlo> res = new HashMap<Long, Grlo>();
		try {
			CSVReader reader = new CSVReader(new FileReader(fajl));
			String[] nextLine;
			int c = 0;
			reader.readNext();// preskacemo header
			while ((nextLine = reader.readNext()) != null) {
				Grlo g = new Grlo(nextLine[colId], nextLine[colOtac],
						nextLine[colMajka], nextLine[colPol]);
				// tipicno nemamo ovo
				if (colRasa >= 0) {
					g.rasa = nextLine[colRasa];
				}
				if (colDatum >= 0) {
					g.datum = nextLine[colDatum];
				}
				ubaci(res, g);
				c++;
			}
			reader.close();
			System.out.println("Ucitano redova:" + c);
			System.out.println("Ucitano grla:" + res.size());
		} catch (IOException e) {
			System.err.println("Greska pri citanju fajla:" + fajl);
			e.printStackTrace();
			System.exit(1);
		}
		return res;
	}

	static void ubaci(HashMap<Long, Grlo> res, Grlo g) {
		if (res.containsKey(g.id)) {
			Grlo grlo = res.get(g.id);
			if (!g.istiIdIRoditelji(grlo))
				System.err
						.println("greska, isto grlo, razliciti roditelji:\n\t"
								+ g + "\n\t" + grlo);
		} else
			res.put(g.id, g);
	}

	private void prosiriUNoviFajl(String stari, String novi) {
		try {
			CSVReader reader = new CSVReader(new FileReader(stari));
			CSVWriter writer = new CSVWriter(new FileWriter(novi));

			String[] header = reader.readNext();
			int duzina = header.length + 3;
			// prosirujemo za 3
			header = Arrays.copyOf(header, duzina);
			header[duzina - 3] = "nId";
			header[duzina - 2] = "nOtac";
			header[duzina - 1] = "nMajka";

			writer.writeNext(header);
			String[] nextLine;
			int c = 0;
			while ((nextLine = reader.readNext()) != null) {
				long gid = Glavni.longIliM1(nextLine[colId]);
				long gido = Glavni.longIliM1(nextLine[colOtac]);
				long gidm = Glavni.longIliM1(nextLine[colMajka]);
				String[] izlaz = Arrays.copyOf(nextLine, duzina);
				izlaz[duzina - 3] = "-1";
				try {
					izlaz[duzina - 3] = renumeracija.get(gid).toString();
				} catch (Exception e) {
					System.err.println("greska: ne postoji renumeracija za:"
							+ gid);
				}
				izlaz[duzina - 2] = "-1";
				try {
					izlaz[duzina - 2] = renumeracija.get(gido).toString();
				} catch (Exception e) {
					System.err.println("greska: ne postoji renumeracija za:"
							+ gido);
				}
				izlaz[duzina - 1] = "-1";
				try {
					izlaz[duzina - 1] = renumeracija.get(gidm).toString();
				} catch (Exception e) {
					System.err.println("greska: ne postoji renumeracija za:"
							+ gidm);
				}
				writer.writeNext(izlaz);
				c++;
			}
			reader.close();
			writer.close();
			System.out.println("Obradjeno redova:" + c);

		} catch (IOException e) {
			System.err.println("Greska pri pisanju iz fajla:" + stari
					+ " u fajl:" + novi);
			e.printStackTrace();
		}

	}
	
	private void dodajGeneracijeUNoviFajl(String stari, String novi) {
		try {
			CSVReader reader = new CSVReader(new FileReader(stari));
			CSVWriter writer = new CSVWriter(new FileWriter(novi));

			String[] noveKolone = {"O","OO","OMO"};
			int brojNovih = noveKolone.length;
			
			String[] header = reader.readNext();
			int duzina = header.length + brojNovih;
			// prosirujemo koliko treba
			header = Arrays.copyOf(header, duzina);
			for (int i=0; i < brojNovih;i++) {
				header[duzina - brojNovih + i] = noveKolone[i];
			}
			
			writer.writeNext(header);
			String[] nextLine;
			int c = 0;
			while ((nextLine = reader.readNext()) != null) {
				long gid = Long.parseLong(nextLine[colId]);
				String[] izlaz = Arrays.copyOf(nextLine, duzina);
				for (int i = 0; i < brojNovih; i++) {
					try {
						izlaz[duzina - brojNovih + i] = nadjiPredka(gid,
								noveKolone[i]);
					} catch (Exception e) {
						izlaz[duzina - brojNovih + i] = "-1";
						// System.err.println("Greska u trazenju predka "+noveKolone[i]+" za "+gid
						// +" : "+e.getClass().getCanonicalName());
					}
				}

				writer.writeNext(izlaz);
				c++;
			}
			reader.close();
			writer.close();
			System.out.println("Obradjeno redova:" + c);

		} catch (IOException e) {
			System.err.println("Greska pri pisanju iz fajla:" + stari
					+ " u fajl:" + novi);
			e.printStackTrace();
		}

	}

	// nalazi predka opisanog stringom (sastoji se od O i M) za dati gid
	private String nadjiPredka(long gid, String string) {
		Grlo g = grla.get(gid);
		switch (string) {
		case "O" :
		case "o" :
			return g.idOtac+"";
		case "OO" :
			return g.otac.idOtac+"";
		case "OMO" :
			return g.otac.majka.otac.id+"";
		default:
			return "-1";
		}
	}

	private void poveziRoditelje() {
		Iterator<Grlo> itg = grla.values().iterator();
		while (itg.hasNext()) {
			Grlo g = itg.next();
			g.otac = grla.get(g.idOtac);
			g.majka = grla.get(g.idMajka);
			if (g.otac != null) {
				g.otac.deca.add(g.id);
			}
			if (g.majka != null) {
				g.majka.deca.add(g.id);
			}
			if (g.majka == null && g.otac == null) {
				// ako nema nijedno u bazi dodati u spisak pocetnih
				bezRoditelja.add(g.id);
			}
			// ako nema jednog od roditelja u bazi treba specijalna obrada tih
			// roditelja
			if (g.otac == null) {
				dodajUBezRoditelja(g.idOtac, Grlo.POL_M);
			}
			if (g.majka == null) {
				dodajUBezRoditelja(g.idMajka, Grlo.POL_Z);
			}

		}

	}

	private void dodajUBezRoditelja(long id, int pol) {
		if (vanBaze.containsKey(id)) {
			// ako je pozitivan id, proveravamo prethodno zapamcen pol
			// inace je neko nepoznatno grlo i za to cemo ignorisati pol
			if (id > 0) {
				int polStari = vanBaze.get(id);
				if (pol != polStari)
					System.err.println("GRESKA: isto grlo van baze kao roditelj razlicitog pola:\t" + id);
			}
		} else
			vanBaze.put(id, pol);
	}

	private void nadjiNoveBrojeve() {
		long brojac = 0;

		ArrayList<Long> al = new ArrayList<>(vanBaze.keySet());
		al.remove(0l);
		for (long i : al) {
			brojac++;
			renumeracija.put(i, brojac);
		}
		// specijalno 0 da ostane 0, to su nepoznate zivotinje!
		renumeracija.put(0l,0l);

		System.out.println("van baze bilo:"+brojac);

		// na pocetku su u redu "bezRoditelja" samo oni koji nemaju roditelje
		System.out.println("bezRoditelja:"+bezRoditelja.size());
		while (!bezRoditelja.isEmpty()) {
			long trenutni = bezRoditelja.remove();
			Grlo g = grla.get(trenutni);

			// dodeli broj
			brojac++;

			renumeracija.put(g.id, brojac);
			// potencijalno se i ovde vec moze sve pamtiti kao novi idovi ako
			// bude trebalo

			Iterator<Long> iti = g.deca.iterator();
			while (iti.hasNext()) {
				Grlo dete = grla.get(iti.next());
				// "uklonimo" ovog roditelja
				if (g.pol == Grlo.POL_Z) {
					if (dete.idMajka != g.id) {
						System.err.print("grlo" + g.id
								+ "pol Z i nije 'majka' deteta " + dete.id);
						if (dete.idOtac == g.id) {
							System.err.print("; jeste 'otac', obradjeno tako");
							dete.otac = null;
						}
						System.err.println();
					}
					dete.majka = null;
				} else if (g.pol == Grlo.POL_M) {
					if (dete.idOtac != g.id) {
						System.err.print("grlo " + g.id
								+ " pol M i nije 'otac' deteta " + dete.id);
						if (dete.idMajka == g.id) {
							System.err.print("; jeste 'majka', obradjeno tako");
							dete.majka = null;
						}
						System.err.println();
					}
					dete.otac = null;
				} else {
					System.err.println("nepoznat pol pri obradi:" + g.pol);
				}
				// ubacujemo dete "bez roditelja" na obradu
				if (dete.majka == null && dete.otac == null) {
					bezRoditelja.add(dete.id);
				}
			}
		}
	}
	
	public boolean proveriCiklus(long id) {
		HashSet<Long> vidjeni = new HashSet<>();
		LinkedList<Long> obilazak = new LinkedList<>();
		
		obilazak.add(id);
		while (!obilazak.isEmpty()) {
			long trenutni = obilazak.remove();

			vidjeni.add(trenutni);
			
			Grlo g = grla.get(trenutni);
			if (g.otac != null) {
				if (g.idOtac == id) {
					System.err.println(String.format("Nadjen ciklus za grlo:%d:grla:%s",id, vidjeni));
					return true;
				}
				if (!vidjeni.contains(g.idOtac))
						obilazak.add(g.idOtac);
			}
			if (g.majka != null) {
				if(!vidjeni.contains(g.idMajka)) 
					obilazak.add(g.idMajka);
				if (g.idMajka == id) {
					System.err.println(String.format("Nadjen ciklus za grlo:%d:grla:%s",id, vidjeni));
					return true;
				}
			}
		}
		return false;
	}
	
	// treba da nadje da li je nesto ostalo posle renumeracije
	public List<Long> nadjiPreostale() {
		LinkedList<Long> l = new LinkedList<>();
		for (Grlo g: grla.values()) {
			if (g.majka != null || g.otac != null) {
				//System.err.println(String.format("Nerenumerisano;%d;%d;%d",g.id, g.idOtac, g.idMajka));
				l.add(g.id);
			}
		}
		return l;
	}

	private void dodajGrloIPretke(HashSet<Grlo> izlaz, Long l,
			Set<String> rase) {
		Grlo g = grla.get(l);
		if (g != null && proveriRase(rase, g) && !izlaz.contains(g)) {
			izlaz.add(g);
			dodajGrloIPretke(izlaz, g.idOtac, rase);
			dodajGrloIPretke(izlaz, g.idMajka, rase);
		}
		
	}

	private boolean proveriRase(Set<String> rase, Grlo g) {
		if (rase == null)
			return true;
		return rase.contains(g.rasa);
	}

	private void ispisiKratko(Set<Grlo> porekla, String novi) {
		try (PrintWriter out = new PrintWriter(
				Files.newBufferedWriter(Paths.get(novi)))) {
			for (Grlo g :porekla) {
				out.println(g.kratkiIspis());
			}
			System.out.println("Porekla ispisana u fajl:"+novi);
		} catch (IOException e) {
			System.err.println("problem pri pisanju u fajl:" + novi);
			e.printStackTrace();
		}
		
	}

	/**
	 * Izdvaja iz trenutno formiranog grafa porekla zivotinje sa id brojevima iz
	 * skupa `baze`, a potom trazi njihove pretke, dokle god je zadovoljeno da
	 * ti preci pripadaju rasama u skupu `rase`. Ako je skup `rase` prosledjen
	 * kao `null` vrednost, onda se dozvoljavaju sve rase.
	 *
	 * @param baze
	 * @param rase
	 * @return
	 */
	private Set<Grlo> izdvojPredke(Set<Long> baze, Set<String> rase) {
		HashSet<Grlo> izlaz = new HashSet<>();
		
		for (Long l : baze) {
			dodajGrloIPretke(izlaz, l, rase);
		}
		
		return izlaz;
	}

	public static String napraviNovoIme(String inname) {
		return Glavni.napraviNovoIme(inname, "-renum");
	}

	public void run(String[] args) {
		if (args.length == 0){
			pomoc();
			System.exit(0);
		}
		Glavni.ucitajOpcije();
		srediKolone();
		
		String fajl = args[0];
		if (args[0].compareToIgnoreCase("--demo")==0) {
			fajl = Glavni.DEF_FAJL;
		}
		
		ucitajIPovezi(fajl);

		nadjiNoveBrojeve();
		System.out.println("renumerisano:" + renumeracija.size());
		try {
			PrintWriter rout = new PrintWriter("renumeracija.txt");
			Glavni.ispisiMapu(renumeracija, rout);
			rout.close();
			System.out.println("renumeracija ispisana u 'renumeracija.txt'");
		} catch (FileNotFoundException e) {
			System.err.println("problem pri ispisu renumeracije");
			e.printStackTrace();
		}
		
		List<Long> l = nadjiPreostale();
		if (l.size() > 0) {
			System.out.println("Ukupno nerenumerisanih:" + l.size());
			System.out.println("Trazimo cikluse, tj da je grlo samo sebi predak:");
			boolean imaCiklusa = false;
			for (long num : l) {
				if (proveriCiklus(num))
					imaCiklusa = true;
			}
			if (!imaCiklusa) {
				System.out.println("Nisu nadjeni ciklusi");
			}
		}
		
		String novi = napraviNovoIme(fajl);
		prosiriUNoviFajl(fajl, novi);
		System.out.println("Novi csv snimljen u "+novi);
		
	}
	
	public void runDodavanjeGeneracija(String[] args){
		if (args.length == 0){
			pomocDodajGeneracije();
			System.exit(0);
		}
		Glavni.ucitajOpcije();
		srediKolone();
		
		String fajl = args[0];
		if (args[0].compareToIgnoreCase("--demo")==0) {
			fajl = Glavni.DEF_FAJL;
		}
		
		ucitajIPovezi(fajl);
		
		String novi = Glavni.napraviNovoIme(fajl, "-generacije");
		dodajGeneracijeUNoviFajl(fajl, novi);
		System.out.println("Novi csv snimljen u "+novi);
	}
	
	public void runPorekloBaze(String[] args) {
		if (args.length < 2){
			PorekloBaza.pomoc();
			System.exit(0);
		}
		Glavni.ucitajOpcije();
		srediKolone();
		
		String fajlsasvima = args[0];
		String fajlbaza = args[1];
		
		ucitajIPovezi(fajlsasvima);
		
		Set<Long> baze = new HashSet<>();
		Set<String> rase = new HashSet<>();
		ucitajIdove(fajlbaza, baze, rase);

		boolean koristiRase = Glavni.booleanIzOpcija(FixId.OPCIJE_PROVERI_RASU,
				true);

		// mozda je prosledjen parametar, to je jace od opcija!
		if (args.length > 2 && "-bezrasa".equals(args[2])) {
			koristiRase = false;
		}

		if (!koristiRase) {
			rase = null;
			System.out.println("Ignorisemo rase kod predaka");
		}

		Set<Grlo> porekla = izdvojPredke(baze, rase);
		
		String novi = Glavni.napraviNovoIme(fajlbaza, "-porekla");
		ispisiKratko(porekla, novi);
	}

	// ucitava i pravi skup id-ova; pretpostavljamo da je slicno formatiran fajl
	private void ucitajIdove(String fajl, Set<Long> idovi, Set<String> rase) {
		System.out.println("Ucitavanje baza (osnova za poreklo)");
		try {
			CSVReader reader = new CSVReader(new FileReader(fajl));
			String[] nextLine;
			int c = 0;
			reader.readNext();// preskacemo header
			while ((nextLine = reader.readNext()) != null) {
				long id = Long.parseLong(nextLine[colId]);
				idovi.add(id);
				//rase.add(nextLine[colRasa]);
				Grlo grlo = grla.get(id);
				if (grlo != null)
					rase.add(grlo.rasa);
				else
					System.err.println("Bazno grlo nije u q-fajlu:" + id);
				c++;
			}
			reader.close();
			System.out.println("Ucitano redova:" + c);
			System.out.println("Ucitano grla:" + idovi.size());
		} catch (IOException e) {
			System.err.println("Greska pri citanju fajla:" + fajl);
			e.printStackTrace();
			System.exit(1);
		}
	}

	private void ucitajIPovezi(String fajl) {
		grla = ucitajIzFajla(fajl);
		poveziRoditelje();
		try {
			PrintWriter gout = new PrintWriter("povezanost.txt");
			Glavni.ispisiKolekciju(grla.values(), gout);
			gout.close();
		} catch (FileNotFoundException e1) {
			System.err.println("problem pri ispisu povezanosti");
		}
	}
	
	private void srediKolone() {
		colId = Glavni.brojKoloneIzOpcija(OPCIJE_COL_ID,COL_ID);
		colOtac = Glavni.brojKoloneIzOpcija(OPCIJE_COL_ID_OTAC,COL_ID_OTAC);
		colMajka = Glavni.brojKoloneIzOpcija(OPCIJE_COL_ID_MAJKA,COL_ID_MAJKA);
		colPol = Glavni.brojKoloneIzOpcija(OPCIJE_COL_POL,COL_POL);
		colRasa = Glavni.brojKoloneIzOpcija(OPCIJE_COL_RASA, COL_RASA);
		colDatum = Glavni.brojKoloneIzOpcija(OPCIJE_COL_DATUM, COL_DATUM);
	}

	private void pomoc() {
		System.out.println("Program FixId;");		
		System.out.println("Fajl koji se obradjuje se zadaje iz komandne linije:");
		System.out.println("\tFixId imefajla.csv");
		System.out.println("Izlaz:");
		System.out.println("- 'imefajla-renum.csv' kao ulaz, sa nove tri kolone nId, nOtac i nMajka ");
		System.out.println("- 'renumeracija.txt' sadrzi stare i nove brojeve za sva grla");
		System.out.println("- 'povezanost.txt' sadrzi mrezu roditelja i dece koja se koristi (informativno)");
		System.out.println();
		System.out.println("Program prima CSV sa podacima o trkama konja i prepravlja id-ove tako da su");		
		System.out.println("roditelji uvek manji od dece.  Autor Doni Pracner");
		
	}

	private void pomocDodajGeneracije() {
		System.out.println("Program DodajGeneracije;");		
		System.out.println("Fajl koji se obradjuje se zadaje iz komandne linije:");
		System.out.println("\tDodajGeneracije imefajla.csv");
		System.out.println("Izlaz:");
		System.out.println("- 'imefajla-generacije.csv' kao ulaz, sa novim kolonama ");
		System.out.println();
		System.out.println("Program prima CSV sa podacima o grlima i id-ovima roditelja, i dopisuje");		
		System.out.println("dodatne generacije pedigrea.  Autor Doni Pracner");
		
	}

	public static void main(String[] args) {
		new FixId().run(args);
	}
}
