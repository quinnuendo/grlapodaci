package com.quemaster.konji;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.TreeMap;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;

import au.com.bytecode.opencsv.CSVWriter;

/*
 Copyright (C) 2015 Doni Pracner

 This file is part of grla-id-fix.

 grla-id-fix is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 grla-id-fix is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with grla-id-fix.  If not, see <http://www.gnu.org/licenses/>.
 */
public class RTFPedigrei {
	private static final String RTF_INPUT_ENCODING = "windows-1252";
	RTFEditorKit kit = new RTFEditorKit();
	String[] header = new String[] {
			"Ime", "Pol", "datum", "mesto", "id",
			"vrsta", "oznake", 
			"otac", "majka", 
			"oo", "om", "mo", "mm", 
			"ooo", "oom", "omo", "omm",
			"moo", "mom", "mmo", "mmm",
			"oooo", "ooom", "oomo", "oomm",
			"omoo", "omom", "ommo", "ommm",
			"mooo", "moom", "momo", "momm",
			"mmoo", "mmom", "mmmo", "mmmm",
			};
	public static final String POPUNJENIH_POLJA="Popunjenih polja";
	
	/**
	 * Predstavlja jedno polje u RTFu, ime polja, u kom je redu, koliko tabova
	 * je udaljeno i da li bi trebalo odsecati bilo sta u zagradama na kraju.
	 */
	class OpisPolja {
		public String ime;
		public int red;
		public int tab;
		public boolean odseciZagrade=true;
		
		public OpisPolja(String ime, int red, int tab) {
			this.ime = ime;
			this.red = red;
			this.tab = tab;
		}

		public OpisPolja(String ime, int red, int tab, boolean odseciZagrade) {
			this.ime = ime;
			this.red = red;
			this.tab = tab;
			this.odseciZagrade = odseciZagrade;
		}		
	}

	private HashMap<String, OpisPolja> opisiPolja;
	
	public HashMap<String, OpisPolja> getOpisiPolja() {
		if (opisiPolja==null) opisiPolja = podrazumevaniOpisiPolja();
		return opisiPolja;
	}

	public void setOpisiPolja(HashMap<String, OpisPolja> opisiPolja) {
		this.opisiPolja = opisiPolja;
	}	
	
	/** Pravi mapu podrazumevanih polja. */
	public HashMap<String, OpisPolja> podrazumevaniOpisiPolja(){
		HashMap<String, OpisPolja> res = new HashMap<>();
		
		res.put("Ime", new OpisPolja("Ime",0,1,false));
		res.put("Pol", new OpisPolja("Pol",1,1,false));

		res.put("datum", new OpisPolja("datum",2,1,false));
		res.put("mesto", new OpisPolja("mesto",2,2,false));

		res.put("id", new OpisPolja("id",3,1,false));
		res.put("vrsta", new OpisPolja("vrsta",4,1,false));
		res.put("oznake", new OpisPolja("oznake",5,1,false));
		
		res.put("otac", new OpisPolja("otac",11,1,false));
		res.put("oo", new OpisPolja("oo",8,1,false));
		res.put("om", new OpisPolja("om",14,1,false));

		res.put("ooo", new OpisPolja("ooo",6,1,false));
		res.put("oooo", new OpisPolja("oooo",6,2));
		res.put("ooom", new OpisPolja("ooom",7,2));

		res.put("oom", new OpisPolja("oom",9,1,false));
		res.put("oomo", new OpisPolja("oomo",9,2));
		res.put("oomm", new OpisPolja("oomm",11,3));

		res.put("omo", new OpisPolja("omo",12,2,false));
		res.put("omoo", new OpisPolja("omoo",12,3));
		res.put("omom", new OpisPolja("omom",13,2));

		res.put("omm", new OpisPolja("omm",14,2,false));
		res.put("ommo", new OpisPolja("ommo",14,3));
		res.put("ommm", new OpisPolja("ommm",16,1));

		res.put("majka", new OpisPolja("majka",21,1,false));
		res.put("mo", new OpisPolja("mo",19,1,false));
		res.put("mm", new OpisPolja("mm",25,1,false));

		res.put("moo", new OpisPolja("moo",17,1,false));
		res.put("mooo", new OpisPolja("mooo",17,2));
		res.put("moom", new OpisPolja("moom",7,2));

		res.put("mom", new OpisPolja("mom",19,2,false));
		res.put("momo", new OpisPolja("momo",19,3));
		res.put("momm", new OpisPolja("momm",21,3));

		res.put("mmo", new OpisPolja("mmo",23,1,false));
		res.put("mmoo", new OpisPolja("mmoo",23,2));
		res.put("mmom", new OpisPolja("mmom",24,2));

		res.put("mmm", new OpisPolja("mmm",26,1,false));
		res.put("mmmo", new OpisPolja("mmmo",26,2));
		res.put("mmmm", new OpisPolja("mmmm",28,2));

		res.put("osoba1", new OpisPolja("osoba1",29,1,false));
		res.put("osoba2", new OpisPolja("osoba2",30,1,false));
		return res;
	}
	
	public TreeMap<String, String> konjIzTeksta(String text) {
		TreeMap<String, String> konj = new TreeMap<String, String>();
		String[] lines = text.split("\n");
		
		// kreiramo "matricu" polja radzvojenih tabovima po redovima
		String[][] tabbed = new String[lines.length][4];
		for (int i = 0; i < lines.length; i++)
			tabbed[i] = lines[i].split("\t");

		HashMap<String, OpisPolja> opisi = getOpisiPolja();
		
		try {
			int counter = 0;
			for (OpisPolja opis : opisi.values()) {
				String rezultat = null;
				if (tabbed.length > opis.red && tabbed[opis.red].length > opis.tab) {
					rezultat = tabbed[opis.red][opis.tab];
					if (opis.odseciZagrade)
						rezultat = odseciZagrade(rezultat);
					konj.put(opis.ime, rezultat);
					counter++;
				} else
					System.err.println("| problem u obradi podataka o konju"
							+ " red:" + opis.red + "\ttab:" + opis.tab
							+ "\tpolje:" + opis.ime);
			}
			konj.put(POPUNJENIH_POLJA, counter+"");
		} catch (Exception ex) {
			System.err.println("problem u obradi podataka o konju");
			ex.printStackTrace();
		}

		return konj;
	}

	private String odseciZagrade(String ulaz) {
		if (ulaz != null) {
			int i = ulaz.lastIndexOf('(');
			if (i >= 0) {
				while (i > 0 && ulaz.charAt(i - 1) == ' ')
					i--;
				return ulaz.substring(0, i);
			}
		}
		return ulaz;
	}

	public String tekstIzRTF(Path rtf) {
		Document doc = kit.createDefaultDocument();
		String text = null;
		try {
			kit.read(Files.newBufferedReader(rtf, Charset.forName(RTF_INPUT_ENCODING)), doc, 0);
			text = doc.getText(0, doc.getLength());
		} catch (IOException | BadLocationException e1) {
			System.err.println("Problem pri citanju RTF fajla:"
					+ rtf.toString());
			e1.printStackTrace();
		}
		return text;
	}

	public void obradiFolder(Path folder, Path csv, Path csvLosi) {
		if (Files.isDirectory(folder)) {

			try (DirectoryStream<Path> stream = Files.newDirectoryStream(
					folder, "*.{rtf,RTF}")) {
				System.out.println("otvaram folder");
				CSVWriter pisac = new CSVWriter(Files.newBufferedWriter(csv,
						Charset.defaultCharset()));
				CSVWriter pisacLosih = new CSVWriter(Files.newBufferedWriter(csvLosi,
						Charset.defaultCharset()));
				pisac.writeNext(header);
				pisacLosih.writeNext(header);
				int potrebanBrojPolja = getOpisiPolja().size();
				int counter = 0;
				int counterL = 0;
				for (Path entry : stream) {
					
					TreeMap<String, String> konj = konjIzTeksta(tekstIzRTF(entry));
					String strbrpolja = konj.get(POPUNJENIH_POLJA);
					int brojpolja = 0;
					if (strbrpolja != null)
						try {
							brojpolja = Integer.parseInt(strbrpolja);
						} catch (Exception e) {
							// nema sta da se uradi
						}
					
					if (brojpolja != potrebanBrojPolja){ 
						System.err.println("-u fajlu-"+entry.toString()+brojpolja);
						System.err.println();
						pisacLosih.writeNext(pretvoriUNiz(konj));
						counterL++;
					} else {
						pisac.writeNext(pretvoriUNiz(konj));
						counter++;
					}
				}
				System.out.print("ukupno obradjeno fajlova:" + (counter+counterL));
				System.out.println(" potpunih: " + counter+" nepotpunih:"+counterL);
				System.out.println("Potpuni rezultati su u: " + csv.toString());
				System.out.println("Nepotpuni rezultati su u: " + csvLosi.toString());
				pisac.close();
				pisacLosih.close();

			} catch (IOException e) {
				System.err.println("greska pri obradi direktorijuma");
				e.printStackTrace();
			}
		} else {
			System.err.println("Prosledjeni parametar nije direktorijum:"
					+ folder.toString());
		}
	}

	private String[] pretvoriUNiz(TreeMap<String, String> konj) {
		String[] res = new String[header.length];
		int i = 0;
		for (String s : header) {
			String s2 = konj.get(s);
			if (s2 != null)
				res[i++] = s2;
			else
				res[i++] = "??????";
		}
		return res;
	}

	public void run(String[] args) {
		if (args.length == 0) {
			pomoc();
			System.exit(0);
		}
		if (args[0].compareToIgnoreCase("--single")!=0){
			Path folder = Paths.get(args[0]);
			Path izlaz = Paths.get(folder.toString() + ".csv");
			Path izlaz2 = Paths.get(folder.toString() + "-nepotpuni" + ".csv");
			obradiFolder(folder, izlaz, izlaz2);
		} else {
			String t = tekstIzRTF(Paths.get(args[1]));
			System.out.println(t);
			System.out.println(konjIzTeksta(t));
		}
	}

	private void pomoc() {
		System.out.println("Program RTFPedigrei;");		
		System.out.println("Folder koji se obradjuje se zadaje iz komandne linije:");
		System.out.println("\tRTFPedigrei folder");
		System.out.println("u 'imefoldera.csv' snima podatke iz svih rtf fajlova");
		System.out.println();
		System.out.println("Alternativno se moze koristi za pojedinacni fajl:");
		System.out.println("\tRTFPedigrei --single imefajla.rtf");
		System.out.println("ispisuje na ekran podatke o datom rtf-u i kako su prepoznati podaci");
		System.out.println("Autor Doni Pracner");

	}

	public static void main(String[] args) {
		new RTFPedigrei().run(args);
		
	}

}
