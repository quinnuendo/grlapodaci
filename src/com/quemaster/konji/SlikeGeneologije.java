/*
Copyright (C) 2020  Doni Pracner

 This file is part of grla-id-fix.

 grla-id-fix is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 grla-id-fix is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with grla-id-fix.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.quemaster.konji;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;

public class SlikeGeneologije {
	private String formatGrlo = "[label=<%2s<br/><font point-size=\"11\">%3s</font>>];%n";
	private String formatCiljnoGrlo = "[label=<<b><font color=\"blue\">%2s</font></b><br/><font point-size=\"11\">%3s</font>>];%n";
	private String formatPocetno = "[shape=underline,label=<<B>%2s</B><br/>%3s>];%n";
	private String opcijeGrafa = "";
	public String opcijeGrafaA4 = "# trazimo da se crta na A4 (u inchima)\n ratio=\"auto\";\n size=\"8.3,11.7\";\n";
	private static final String KOMPLET_GRLO = "komplet_grlo";
	private static final String KOMPLET_OTAC = "komplet_otac";
	private static final String KOMPLET_MAJKA = "komplet_majka";
	private int colId;
	private int colMajka;
	private int colGME;
	private int colGod;
	private int colKomplet = 4;
	private int colKompletMajka = 6;
	private int colKompletOtac = 5;
	private Grlo pocetno;
	private Set<Grlo> ciljnaGrla = new HashSet<Grlo>();
	private Map<Long, Grlo> grla;
	private Map<Long, Properties> grlaDodatno = new HashMap<Long, Properties>();
	static final int COL_ID = 1;
	static final int COL_ID_OTAC = -1;
	static final int COL_ID_MAJKA = 2;
	static final int COL_POL = -1;
	static final int COL_GME = 4;
	static final int COL_GOD = 3;
	private static final String OPCIJE_COL_ID = "sKolonaId";
	private static final String OPCIJE_COL_ID_MAJKA = "sKolonaIdMajka";
	private static final String OPCIJE_COL_GME = "sKolonaGME";
	private static final String OPCIJE_COL_GOD = "sKolonaGOD";
	private LinkedList<Long> bezRoditelja = new LinkedList<Long>();
	private HashMap<Long, Integer> vanBaze = new HashMap<>();

	private void srediKolone() {
		colId = Glavni.brojKoloneIzOpcija(OPCIJE_COL_ID, COL_ID);
		colMajka = Glavni.brojKoloneIzOpcija(OPCIJE_COL_ID_MAJKA, COL_ID_MAJKA);
		colGME = Glavni.brojKoloneIzOpcija(OPCIJE_COL_GME, COL_GME);
		colGod = Glavni.brojKoloneIzOpcija(OPCIJE_COL_GOD, COL_GOD);
	}

	public HashMap<Long, Grlo> ucitajIzFajla(String fajl) {
		HashMap<Long, Grlo> res = new HashMap<Long, Grlo>();
		try {
			CSVReader reader = new CSVReader(Files.newBufferedReader(Paths.get(fajl)));
			String[] nextLine;
			int c = 0;
			reader.readNext();// preskacemo header
			while ((nextLine = reader.readNext()) != null) {
				// Trenutno je ulazni fajl samo sa zenkama
				long id = Long.parseLong(nextLine[colId]);
				Grlo g = new Grlo(nextLine[colId], "-1", nextLine[colMajka],
						Grlo.POL_Z + "");
				g.godRodj = Integer.parseInt(nextLine[colGod]);
//				Grlo g = new Grlo(nextLine[colId], nextLine[colOtac],
//						nextLine[colMajka], nextLine[colPol]);

				FixId.ubaci(res, g);
				postaviOsobinu(id, KOMPLET_GRLO,
						ocistiKomplet(nextLine[colKomplet]));
				postaviOsobinu(id, KOMPLET_OTAC,
						ocistiKomplet(nextLine[colKompletOtac]));
				postaviOsobinu(id, KOMPLET_MAJKA,
						ocistiKomplet(nextLine[colKompletMajka]));

				String gme = nextLine[colGME];
				// provera da li je u pitanju pocetno grlo ili neko od ciljnih
				if ("O".equalsIgnoreCase(gme) || "О".equalsIgnoreCase(gme)) {
					if (pocetno == null)
						pocetno = g;
					else {
						System.err.println(
								"Vise od jednog grla kandidati za pocetno (osnivaca)");
						System.err.println("Novo nadjeno:" + g);
						System.err.println(
								"Koristice se prethodno nadjeno:" + pocetno);
					}
				}
				if (gme.contains("RB") || gme.contains("HB"))
					ciljnaGrla.add(g);
				c++;
			}
			reader.close();
			System.out.println("Ucitano redova:" + c);
			System.out.println("Ucitano grla:" + res.size());
		} catch (IOException e) {
			System.err.println("Greska pri citanju fajla:" + fajl);
			e.printStackTrace();
			System.exit(1);
		}
		return res;
	}

	private void postaviOsobinu(long id, String osobina, String vrednost) {
		Properties p = grlaDodatno.get(id);
		if (p == null) {
			p = new Properties();
			grlaDodatno.put(id, p);
		}
		p.setProperty(osobina, vrednost);
	}

	private String ocistiKomplet(String komplet) {
		return komplet.replace("/ ???", "").replace("/ 0", "");
	}

	private String nadjiOsobinu(long id, String osobina) {
		Properties p = grlaDodatno.get(id);
		if (p == null)
			return null;
		return p.getProperty(osobina);
	}

	private void poveziRoditelje() {
		Iterator<Grlo> itg = grla.values().iterator();
		while (itg.hasNext()) {
			Grlo g = itg.next();
			g.otac = grla.get(g.idOtac);
			g.majka = grla.get(g.idMajka);
			if (g.otac != null) {
				g.otac.deca.add(g.id);
			}
			if (g.majka != null) {
				g.majka.deca.add(g.id);
			}
			if (g.majka == null && g.otac == null) {
				// ako nema nijedno u bazi dodati u spisak pocetnih
				bezRoditelja.add(g.id);
			}
			// ako nema jednog od roditelja u bazi treba specijalna obrada tih
			// roditelja
			if (g.otac == null) {
				dodajUBezRoditelja(g.idOtac, Grlo.POL_M);
			}
			if (g.majka == null) {
				dodajUBezRoditelja(g.idMajka, Grlo.POL_Z);
			}

		}

	}

	private void dodajUBezRoditelja(long id, int pol) {
		if (vanBaze.containsKey(id)) {
			// ako je pozitivan id, proveravamo prethodno zapamcen pol
			// inace je neko nepoznatno grlo i za to cemo ignorisati pol
			if (id > 0) {
				int polStari = vanBaze.get(id);
				if (pol != polStari)
					System.err.println(
							"GRESKA: isto grlo van baze kao roditelj razlicitog pola:\t"
									+ id);
			}
		} else
			vanBaze.put(id, pol);
	}

	private void nacrtajGrafPoMajcinojLiniji(String filename) {
		PrintWriter out = null;
		if (filename != null) {
			try {
				out = new PrintWriter(
						Files.newBufferedWriter(Paths.get(filename)));
			} catch (IOException e) {
				System.err.println("Problem pri otvaranju fajla:" + filename);
				System.exit(1);
			}
		} else {
			out = new PrintWriter(System.out);
		}
		HashSet<Grlo> naSlici = new HashSet<>();
		out.println("digraph Pedigre {");

		out.println(opcijeGrafa);

		out.printf("\t%1d " + formatPocetno, pocetno.id,
				nadjiOsobinu(pocetno.id, KOMPLET_GRLO),
				nadjiOsobinu(pocetno.id, KOMPLET_OTAC),
				nadjiOsobinu(pocetno.id, KOMPLET_MAJKA));
		naSlici.add(pocetno);

		// out.println(" node [shape=rectangle];");
		out.println("\tnode [shape=plaintext];");
		for (Grlo g : ciljnaGrla) {
			out.printf("\t%1d " + formatCiljnoGrlo, g.id,
					nadjiOsobinu(g.id, KOMPLET_GRLO),
					nadjiOsobinu(g.id, KOMPLET_OTAC),
					nadjiOsobinu(g.id, KOMPLET_MAJKA));
		}

		for (Grlo cilj : ciljnaGrla) {
			LinkedList<Grlo> put = poveziSaPocetnim(cilj);
			if (put == null || put.size() == 0) {
				System.err.println("greska, ne moze se naci put za " + cilj);
			} else {
				for (Grlo g : put) {
					if (!naSlici.contains(g)) {
						if (!ciljnaGrla.contains(g))
							out.printf("\t%1d" + formatGrlo, g.id,
									nadjiOsobinu(g.id, KOMPLET_GRLO),
									nadjiOsobinu(g.id, KOMPLET_OTAC),
									nadjiOsobinu(g.id, KOMPLET_MAJKA));
						out.printf("\t%1d -> %2d;%n", g.idMajka, g.id);
						naSlici.add(g);
					}
				}
			}
		}
		out.println("}");
		out.close();
	}

	// trazi put, ali iskljucivo preko majke, vratice null ako nema puta
	private LinkedList<Grlo> poveziSaPocetnim(Grlo g) {
		LinkedList<Grlo> put = new LinkedList<>();
		if (g == null)
			return null;
		Grlo t = g;
		while (t != null && t != pocetno) {
			put.addFirst(t);
			t = t.majka;
		}
		if (t != pocetno) {
			return null;
		} else {
			put.addFirst(pocetno);
			return put;
		}
	}

	public void pomoc() {
		System.out.println(
				"Program za pravljanje crteza povezanosti aktivnih grla sa osnivacem");
		System.out.println("\tOpcija '--a4' trazi da se crta na a4 listu");
		System.out.println(
				"\tOpcija '-f format.txt' ucitava formatiranje grala iz fajla (videti pomoc)");
		System.out.println(
				"\tOcekuje se parametar koji je ima CSV fajla iz koga se cita");
		System.out.println(
				"\tDrugi parametar je ime izlaznog fajla; ako nije dato ispisace se na ekran");
	}

	public void run(String[] args) {
		if (args.length == 0) {
			pomoc();
			System.exit(0);
		}
		String ulazni = "kobile.csv";
		String izlazni = null;
		int ac = 0;
		if (ac < args.length && args[ac].equalsIgnoreCase("--a4")) {
			ac++;
			opcijeGrafa = opcijeGrafaA4;
		}
		if (ac < args.length && args[ac].equalsIgnoreCase("-f")) {
			ac++;
			if (ac < args.length) {
				podesiFormatiranjeIzFajla(args[ac]);
			} else {
				System.err.println(
						"Nakon '-f' je potrebno ime fajla sa formatiranjem");
				System.exit(1);
			}
			ac++;
		}
		if (ac < args.length) {
			ulazni = args[ac];
			ac++;
		}
		if (ac < args.length) {
			izlazni = args[ac];
			ac++;
		}
		srediKolone();
		grla = ucitajIzFajla(ulazni);

		if (pocetno == null) {
			System.err.println(
					"Nije nadjeno pocetno grlo (osnivac), tj GME oznaka O");
			System.exit(1);
		}

		poveziRoditelje();

		System.out.println("Pocetno grlo:" + pocetno.id);
		System.out.print("Ciljna grla:");
		for (Grlo g : ciljnaGrla) {
			System.out.print(g.id + " ");
		}
		System.out.println();
		nacrtajGrafPoMajcinojLiniji(izlazni);
	}

	private void podesiFormatiranjeIzFajla(String string) {
		try (BufferedReader r = Files.newBufferedReader(Paths.get(string))) {
			formatPocetno = r.readLine() + " %n";
			formatGrlo = r.readLine() + " %n";
			formatCiljnoGrlo = r.readLine() + " %n";

			// ostatak fajla cemo koristiti za pocetak grafa

			String red = r.readLine();
			if ("".equals(red))
				// preskacemo prazan red
				red = r.readLine();
			StringBuilder sb = new StringBuilder();
			while (red != null) {
				sb.append(red);
				sb.append("\n");
				red = r.readLine();
			}
			opcijeGrafa = sb.toString();
		} catch (IOException e) {
			System.err.println("Greska pri citanju fajla sa formatiranjem");
			e.printStackTrace();
			System.exit(1);
		}

	}

	public static void main(String[] args) {
		new SlikeGeneologije().run(args);
	}

}
