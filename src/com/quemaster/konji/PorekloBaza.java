package com.quemaster.konji;

/*
Copyright (C) 2021,2022  Doni Pracner

 This file is part of grla-id-fix.

 grla-id-fix is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 grla-id-fix is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with grla-id-fix.  If not, see <http://www.gnu.org/licenses/>.
*/
public class PorekloBaza {
	
	public static void pomoc() {
		System.out.println("Program PorekloBaza");
		System.out.println("Potrebna su dva fajla - jedan je pedigre ('q' fajl), drugi spisak grla koji su baza");
		System.out.println("\tPorekloBaza pedigre.csv baze.csv");
		System.out.println("Iz drugog se citaju id-ovi, koristi se ista kolona kao za prvi;");
		System.out.println("Fajlovi inace ne moraju imati ista formatiranja.");
		System.out.println("Moze se proslediti dodatno opcija `-bezrasa`, koja iskljucuje proveru rasa predaka");
	}

	public static void main(String[] args) {
		new FixId().runPorekloBaze(args);
	}

}
