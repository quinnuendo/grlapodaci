package com.quemaster.konji;
/*
Copyright (C) 2014,2015,2021,2022  Doni Pracner

 This file is part of grla-id-fix.

 grla-id-fix is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 grla-id-fix is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with grla-id-fix.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;

public class Glavni {

	private static Properties opcije = new Properties();

	public static Properties Opcije() {
		return opcije;
	}

	public static void ucitajOpcije() {
		try {
			opcije.load(new FileReader("opcije.txt"));
		} catch (IOException e) {
			System.err.println("fajl 'opcije.txt' nije nadjen");
		}
	}

	public static void ispisiKolekciju(Collection<Grlo> s, PrintWriter out) {
		for (Grlo g : s) {
			out.println(g);
		}
	}

	public static void ispisiMapu(HashMap<Long, Long> renumeracija,
			PrintWriter out) {
		SortedSet<Long> s = new TreeSet<Long>(renumeracija.keySet());
		for (long i : s) {
			out.println(i + ";" + renumeracija.get(i));
		}
	}

	public static void ispisiMapu(HashMap<Long, Long> mapa) {
		ispisiMapu(mapa, new PrintWriter(System.out));
	}

	public static void pomoc() {
		System.out.println("Program za rad sa grlima");
		System.out.println("\tFixId [opcije]");
		System.out.println("\tReorganizacija [opcije]");
		System.out.println("\tRTFPedigrei [opcije]");
		System.out.println("\tSpisakOdPedigrea [opcije]");
		System.out.println("\tDodajGeneracije [opcije]");
		System.out.println("\tSlikePedigrea [opcije]");
		System.out.println("\tPorekloBaza [opcije]");
		System.out.println("\tFiksneSirine [opcije]");
		System.out.println(
				"pozvati bilo koji od gore navedenih bez opcija za vise pomoci");
		System.out.println("Autor Doni Pracner");

	}

	public static void main(String[] args) {
		if (args.length == 0) {
			pomoc();
			System.exit(0);
		}
		int curr = 0;
		if (args[curr].compareToIgnoreCase("FixId") == 0) {
			new FixId().run(Arrays.copyOfRange(args, curr + 1, args.length));
		} else if (args[curr].compareToIgnoreCase("Reorganizacija") == 0) {
			new Reorganizacija()
					.run(Arrays.copyOfRange(args, curr + 1, args.length));
		} else if (args[curr].compareToIgnoreCase("RTFPedigrei") == 0) {
			new RTFPedigrei()
					.run(Arrays.copyOfRange(args, curr + 1, args.length));
		} else if (args[curr].compareToIgnoreCase("SpisakOdPedigrea") == 0) {
			new SpisakOdPedigrea()
					.run(Arrays.copyOfRange(args, curr + 1, args.length));
		} else if (args[curr].compareToIgnoreCase("DodajGeneracije") == 0) {
			new FixId().runDodavanjeGeneracija(
					Arrays.copyOfRange(args, curr + 1, args.length));
		} else if (args[curr].compareToIgnoreCase("SlikeGeneologije") == 0) {
			new SlikeGeneologije()
					.run(Arrays.copyOfRange(args, curr + 1, args.length));
		} else if (args[curr].compareToIgnoreCase("PorekloBaza") == 0) {
			PorekloBaza.main(
					Arrays.copyOfRange(args, curr + 1, args.length));
		} else if (args[curr].compareToIgnoreCase("FiksneSirine") == 0) {
			FiksneSirine.main(
					Arrays.copyOfRange(args, curr + 1, args.length));
		} else {
			System.out.println("Neprepoznata opcija \"" + args[0] + "\"");
		}
	}

	static final String DEF_FAJL = "primer/grla.csv";

	public static String napraviNovoIme(String inname, String noviKraj) {
		String rez = inname;
		if (inname.toLowerCase().endsWith(".csv"))
			rez = rez.substring(0, rez.length() - 4);
		return rez + noviKraj + ".csv";
	}

	public static int brojKoloneIzOpcija(String opcijeColId, int colId) {
		// smanjimo za jedan, da bude u fajlu brojanje od 1
		return brojIzOpcija(opcijeColId, colId) - 1;
	}

	public static boolean booleanIzOpcija(String opcija, boolean pozdrazumevana) {
		String s = Glavni.Opcije().getProperty(opcija);
		boolean res = pozdrazumevana;
		if (s != null) {
			// obradimo string,
			if (s.equalsIgnoreCase("Da") || s.equalsIgnoreCase("True")
					|| s.equalsIgnoreCase("Yes"))
				res = true;
			else {
				if (s.equalsIgnoreCase("Ne") || s.equalsIgnoreCase("False")
						|| s.equalsIgnoreCase("No"))
					res = false;
				else
					System.err.println("Neprepoznata vrednost:" + s
							+ " u opcijama za:" + opcija);
			}
		}
		return res;
	}

	public static int brojIzOpcija(String opcija, int pozdrazumevana) {
		String s = Glavni.Opcije().getProperty(opcija);
		int res = pozdrazumevana;
		if (s != null)
			try {
				// obradimo string,
				res = Integer.parseInt(s);
			} catch (NumberFormatException e) {
				System.err.println("Pogresna vrednost u opcijama za:" + opcija);
				e.printStackTrace();
			}
		return res;
	}
	
	public static long longIliM1(String s) {
		try {
			return Long.parseLong(s);
		} catch (NumberFormatException e) {
			return -1;
		}
	}
	
	public static int intIliM1(String s) {
		try {
			return Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return -1;
		}
	}	
}
