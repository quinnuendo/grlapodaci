U ovom folderu se nalazi nekoliko primera ulaznih fajlova.
Takodje su dati i neki izlazi koji se mogu ocekivati.


grla.csv
----------

mali primer sa 100 redova i nekoliko konja. U okviru
fajla se nalazi dosta podataka o trkama.

Ovaj fajl je podrazumevani ulaz koji se pominje u glavnom
fajlu README.txt u korenskom folderu, odnosno bitne
kolone su 1,2,7 i 8.

Dati su izlazi grla-renum.csv i grla-bi.csv

Takodje su dati renumeracija.txt i povezanost.txt
za ovaj fajl.

Dat je i fajl brojeviOsobina.txt koji je izlaz pri operaciji
reorganizacija.


minimalni.csv
-------------

isti primer kao gore, ali minimalizovan za renumeracije,
odnosno ima samo te 4 bitne kolone.

dat je izlaz minimalni-renum.csv.

za ovaj fajl vaze isti izlazi renumeracija.txt i povezanost.txt
